package br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.inputs;


import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Email;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import java.util.UUID;

@Data
@Schema(description = "Representação de entrada de novo estudante.")

public class StudentInput {
    
    @Schema(description = "Nome do estudante.")
    @NotBlank(message = "Nome não pode ser vazio!")
    private String name;

    @Schema(description = "CPF do estudante.")
    @NotBlank(message = "CPF não pode ser vazio!")
    @Pattern(regexp = "^\\d{3}\\.\\d{3}\\.\\d{3}-\\d{2}$")
    private String cpf;

    @Email
    @Schema(description = "Email do estudante.")
    private String email;

    @Schema(description = "Telefone do estudante.")
    @Pattern(regexp = "^(\\d{2}\\)-\\d{5}\\-\\d{4}$")
    @NotBlank(message = "Celular não pode ser vazio")
    private String phone;

    @Schema(description = "ID do curso do estudante.")
    private UUID courseId;

}