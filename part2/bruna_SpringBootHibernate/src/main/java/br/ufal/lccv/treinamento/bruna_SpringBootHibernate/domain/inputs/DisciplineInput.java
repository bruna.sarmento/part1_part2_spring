package br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.inputs;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
@Schema(description = "Representação de entrada de nova disciplina.")

public class DisciplineInput {

    @Schema(description = "Nome da disciplina.")
    @NotBlank(message = "Nome não pode ser vazio!")
    private String name;

    @Schema(description = "Carga horária da disciplina.")
    @Min(value = 30, message = "A disciplina precisa ter mais de 30 horas")
    private Integer workload;

    @Schema(description = "Sala de Aula da disciplina.")
    @NotBlank(message = "Sala de Aula não pode ser vazio")
    private int Classroom;

    @Schema(description = "Descrição da disciplina.")
    private String description;
}
