package br.ufal.lccv.treinamento.bruna_SpringBootHibernate.repositories;

import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.dtos.DisciplineDTO;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.models.Discipline;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.UUID;

@Repository
public interface DisciplineRepository extends JpaRepository<DisciplineDTO, UUID> {

    Discipline save(Discipline discipline);
}