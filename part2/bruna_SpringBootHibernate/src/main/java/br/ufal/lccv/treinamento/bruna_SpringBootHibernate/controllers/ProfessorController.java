package br.ufal.lccv.treinamento.bruna_SpringBootHibernate.controllers;

import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.models.Professor;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.inputs.ProfessorInput;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.dtos.ProfessorDTO;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.builders.ProfessorBuilder;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.services.ProfessorService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;

import javax.validation.Valid;
import java.util.List;

@Tag(name = "Professor", description = "Endpoints da entidade Professor")
@RestController
@RequestMapping("/professors")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))

public class ProfessorController {

    private final ProfessorService service;
    
    @GetMapping
    public List<ProfessorDTO> getAllProfessor() {
        return service.getAllProfessor();
    }

    @GetMapping("/CPF/{cpf}")
    public ProfessorDTO getOneProfessorCPF(@PathVariable String cpf) {
       return service.getOneProfessorCPF(cpf);
    }

    @GetMapping("/email/{email}")
    public ProfessorDTO getOneProfessorEmail(@PathVariable String email) {
       return service.getOneProfessorEmail(email);
    }

    @PostMapping("/add")
    public ProfessorDTO createStudent(
        @Valid 
        @RequestBody 
        ProfessorInput professorInput) {
        Professor professor = service.addProfessor(professorInput);
        return ProfessorBuilder.build(professor);
    }

}

