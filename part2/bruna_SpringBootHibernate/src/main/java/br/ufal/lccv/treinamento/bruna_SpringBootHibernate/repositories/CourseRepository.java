package br.ufal.lccv.treinamento.bruna_SpringBootHibernate.repositories;

import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.dtos.CourseDTO;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.models.Course;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.UUID;

@Repository
public interface CourseRepository extends JpaRepository<CourseDTO, UUID> {

    Course save(Course course);
}
