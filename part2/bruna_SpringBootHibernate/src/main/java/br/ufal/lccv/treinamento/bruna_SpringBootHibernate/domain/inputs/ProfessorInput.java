package br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.inputs;


import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Email;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
@Schema(description = "Representação de entrada de novo professor.")

public class ProfessorInput {
    
    @Schema(description = "Nome do professor.")
    @NotBlank(message = "Nome não pode ser vazio!")
    private String name;

    @Schema(description = "CPF do professor.")
    @NotBlank(message = "CPF não pode ser vazio!")
    @Pattern(regexp = "^\\d{3}\\.\\d{3}\\.\\d{3}-\\d{2}$")
    private String cpf;

    @Email
    @Schema(description = "Email do professor.")
    private String email;

    @Schema(description = "Telefone do professor.")
    @Pattern(regexp = "^(\\d{2}\\)-\\d{5}\\-\\d{4}$")
    @NotBlank(message = "Celular não pode ser vazio")
    private String phone;

    @Schema(description = "Grau de Formação do professor.")
    @NotBlank(message = "Grau de Formação não pode ser vazio")
    private String DegreeOfTraining;

}
