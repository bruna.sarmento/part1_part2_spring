package br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import java.util.UUID;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class ProfessorDTO {

    private UUID id;
    private String cpf;
    private String name;
    private String email;
    private String phone;
    private String degreeOfTraining;
}

