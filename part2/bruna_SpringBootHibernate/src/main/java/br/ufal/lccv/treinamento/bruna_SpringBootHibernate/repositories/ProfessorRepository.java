package br.ufal.lccv.treinamento.bruna_SpringBootHibernate.repositories;

import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.dtos.ProfessorDTO;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.models.Professor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.UUID;

@Repository
public interface ProfessorRepository extends JpaRepository<ProfessorDTO, UUID> {

    Professor save(Professor professor);
}