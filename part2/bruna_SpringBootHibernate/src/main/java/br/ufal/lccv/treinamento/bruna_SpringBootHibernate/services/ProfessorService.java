package br.ufal.lccv.treinamento.bruna_SpringBootHibernate.services;

import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.models.Professor;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.inputs.ProfessorInput;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.repositories.ProfessorRepository;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.dtos.ProfessorDTO;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.builders.ProfessorBuilder;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))

public class ProfessorService {
    
    private final ProfessorRepository professorRepo;
    
    public List<ProfessorDTO> getAllProfessor(){
        return professorRepo.findAll();
    }

    public ProfessorDTO getOneProfessorCPF(String cpf){
        List<ProfessorDTO> getAllProfessor = professorRepo.findAll();
        for (ProfessorDTO professor : getAllProfessor) {
            if(professor.getCpf() == cpf){
                return professor;
        }   
        }
        return null;
    }

    public ProfessorDTO getOneProfessorEmail(String email){
        List<ProfessorDTO> getAllProfessor = professorRepo.findAll();
        for (ProfessorDTO professor : getAllProfessor) {
            if(professor.getEmail() == email){
                return professor;
        }   
        }
        return null;
    }

    public Professor addProfessor(ProfessorInput professorInput) {
        Professor professor = ProfessorBuilder.build(professorInput);
        professor = professorRepo.save(professor);

        return professor;
    }
}
