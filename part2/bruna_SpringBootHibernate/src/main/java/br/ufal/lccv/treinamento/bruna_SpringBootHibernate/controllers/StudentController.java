package br.ufal.lccv.treinamento.bruna_SpringBootHibernate.controllers;

import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.models.Student;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.inputs.StudentInput;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.dtos.StudentDTO;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.builders.StudentBuilder;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.services.StudentService;

import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.models.Course;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.dtos.CourseDTO;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.builders.CourseBuilder;

import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@Tag(name = "Students", description = "Endpoints da entidade Student")
@RestController
@RequestMapping("/students")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))

public class StudentController {

    private final StudentService service;

    @GetMapping
    public List<Student> getAllStudent() {
        return service.getAllStudent();
    }

    @GetMapping("/CPF/{cpf}")
    public StudentDTO getOneStudentCPF(@PathVariable String cpf) {
       return service.getOneStudentCPF(cpf);
    }

    @GetMapping("/{id}/course")
    public CourseDTO getStudentCourse(@PathVariable UUID id) {
      Course course = service.getStudentCourse(id);
      return CourseBuilder.build(course);
    }

    @PostMapping("/add")
    public StudentDTO createStudent(
        @Valid 
        @RequestBody 
        StudentInput studentInput
        ) {
        Student student = service.addStudent(studentInput);
        return StudentBuilder.build(student);
    }

    @PutMapping("/{cpf}/{courseId}")
    public StudentDTO changeStudentCourse(@PathVariable UUID id, @PathVariable UUID courseId) {
       return service.changeStudentCourse(id, courseId);
    }
   
}