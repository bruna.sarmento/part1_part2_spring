package br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.dtos;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.models.Professor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class DisciplineDTO {

    private UUID disciplineId;
    private String name;
    private Integer workload;
    private int classroom;
    private String description;
    public String getName;
    public Professor getProfessor() {
        return null;
    }
    public void setCourse(ProfessorDTO professor) {
    }
}
