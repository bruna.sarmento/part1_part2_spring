package br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.dtos;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.models.Student;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class CourseDTO {

    private UUID id;
    private String name;
    private String coordinatorId;
    private String creationDate;
    public List<StudentDTO> getStudents() {
        return null;
    }
    public Student getStudent() {
        return null;
    }
    public String getCourse() {
        return null;
    }
}

