package br.ufal.lccv.treinamento.bruna_SpringBootHibernate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BrunaSpringBootHibernateApplication {

	public static void main(String[] args) {
		SpringApplication.run(BrunaSpringBootHibernateApplication.class, args);
	}

}
