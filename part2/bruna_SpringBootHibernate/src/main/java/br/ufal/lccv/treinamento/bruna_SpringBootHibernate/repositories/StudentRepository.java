package br.ufal.lccv.treinamento.bruna_SpringBootHibernate.repositories;

import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.dtos.StudentDTO;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.models.Student;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.UUID;

@Repository
public interface StudentRepository extends JpaRepository<StudentDTO, UUID> {

    void save(Student student);

    void save(UUID id);
}
