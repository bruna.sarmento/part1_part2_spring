package br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.dtos.CourseDTO;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;

import java.util.List;
import java.util.UUID;

@Entity
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor

public class Student {
    
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Type(type="uuid-char")
    private UUID id;

    @ManyToOne
    private Course course;

    @ManyToMany
    @JoinTable(
        name = "student_discipline", 
        joinColumns = @JoinColumn(name = "student_id", referencedColumnName = "id"),
        inverseJoinColumns = @JoinColumn(name = "discipline_id", referencedColumnName = "disciplineId")
    )
    private List<Discipline> disciplines;

    private String cpf;
    private String name;
    private String email;
    private String phone;
    public void setCourse(CourseDTO course2) {
    }   
    
}
