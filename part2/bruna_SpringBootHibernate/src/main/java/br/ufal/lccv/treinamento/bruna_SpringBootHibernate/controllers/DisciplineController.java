package br.ufal.lccv.treinamento.bruna_SpringBootHibernate.controllers;


import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.inputs.DisciplineInput;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.dtos.DisciplineDTO;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.services.DisciplineService;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.v3.oas.annotations.tags.Tag;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@Tag(name = "Dicipline", description = "Endpoints da entidade Dicipline")
@RestController
@RequestMapping("/disciplines")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))

public class DisciplineController {

    private final DisciplineService service;
    
    @GetMapping
    public List<DisciplineDTO> getAllDiscipline() {
        return service.getAllDiscipline();
    }

    @GetMapping("/discipline_name/{name}")
    public DisciplineDTO getOneDiscipline(@PathVariable String name){
        return service.getOneDiscipline(name);
    }

    @PostMapping("/add")
    public DisciplineDTO addDiscipline(
        @Valid 
        @RequestBody 
        DisciplineInput disciplineInput) {
        return service.addDiscipline(disciplineInput);
    } 

    @PutMapping("/professor/{professorId}/{name}")
    public DisciplineDTO changeProfessorDiscipline(
        @PathVariable UUID disciplineId,
        @PathVariable UUID professorId) {
        return service.changeProfessorDiscipline(professorId, disciplineId);
    } 
        
}
