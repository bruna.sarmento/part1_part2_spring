package br.ufal.lccv.treinamento.bruna_SpringBootHibernate.services;


import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.models.Discipline;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.inputs.DisciplineInput;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.repositories.DisciplineRepository;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.dtos.DisciplineDTO;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.dtos.ProfessorDTO;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.builders.DisciplineBuilder;

import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.models.Professor;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.repositories.ProfessorRepository;


import java.util.List;
import java.util.UUID;

import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;


@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))

public class DisciplineService {
 
    private final DisciplineRepository disciplineRepo;
    private final ProfessorRepository professorRepo;

    public List<DisciplineDTO> getAllDiscipline(){
        return disciplineRepo.findAll();
    }

    public DisciplineDTO getOneDiscipline(String name){
        List<DisciplineDTO> getAllDiscipline = disciplineRepo.findAll();
        for (DisciplineDTO discipline : getAllDiscipline) {
            if(discipline.getName() == name){
                return discipline;
        }       
        }
        return null;
    }

    public DisciplineDTO addDiscipline(DisciplineInput disciplineInput) {
        Discipline discipline = DisciplineBuilder.build(disciplineInput);
        discipline = disciplineRepo.save(discipline);

        return DisciplineBuilder.build(discipline);
    }

    public Professor getDisciplineProfessor(UUID id){
        DisciplineDTO discipline = disciplineRepo.getReferenceById(id);
        Professor professor = discipline.getProfessor();
        return professor;
    }     

    public DisciplineDTO changeProfessorDiscipline(UUID professorId, UUID disciplineId){
        ProfessorDTO professor = professorRepo.getReferenceById(professorId);
        DisciplineDTO discipline = disciplineRepo.getReferenceById(professorId);
        discipline.setCourse(professor);
        disciplineRepo.save(discipline);
        return discipline;
    }
}
