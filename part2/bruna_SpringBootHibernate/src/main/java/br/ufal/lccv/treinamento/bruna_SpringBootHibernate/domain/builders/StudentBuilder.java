package br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.builders;

import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.models.Student;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.inputs.StudentInput;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.dtos.StudentDTO;

public class StudentBuilder {
    
    public static Student build(StudentInput input) {
        return Student.builder()
                .cpf(input.getCpf())
                .name(input.getName())
                .email(input.getEmail())
                .phone(input.getPhone())
                .build();
    }
  
    public static StudentDTO build(Student student) {
        return (StudentDTO.builder())              
                .cpf(student.getCpf())
                .name(student.getName())
                .studentId(student.getId())
                .build();
    }
}

