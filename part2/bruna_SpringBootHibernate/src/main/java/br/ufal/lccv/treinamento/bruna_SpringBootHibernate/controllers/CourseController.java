package br.ufal.lccv.treinamento.bruna_SpringBootHibernate.controllers;

import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.models.Course;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.inputs.CourseInput;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.dtos.CourseDTO;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.builders.CourseBuilder;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.services.CourseService;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.v3.oas.annotations.tags.Tag;

import javax.validation.Valid;
import java.util.List;

@Tag(name = "Course", description = "Endpoints da entidade Course")
@RestController
@RequestMapping("/courses")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))

public class CourseController {

    private final CourseService service;
    
    @GetMapping
    public List<CourseDTO> getAllCourse() {
        return service.getAllCourse();
    }

    @GetMapping("/course_name/{courseName}")
    public CourseDTO getOneCourse(@PathVariable String name) {
       return service.getOneCourse(name);
    }

    @PostMapping("/add")
    public CourseDTO addCourse(
        @Valid 
        @RequestBody 
        CourseInput courseInput) {
        Course course = service.addCourse(courseInput);
        return CourseBuilder.build(course);
    } 

     
}
