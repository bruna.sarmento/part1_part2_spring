package br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.builders;

import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.models.Professor;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.inputs.ProfessorInput;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.dtos.ProfessorDTO;

public class ProfessorBuilder {
        
    public static Professor build(ProfessorInput input) {
        return Professor.builder()
                .cpf(input.getCpf())
                .name(input.getName())
                .email(input.getEmail())
                .phone(input.getPhone())
                .degreeOfTraining(input.getDegreeOfTraining())
                .build();
    }

    public static ProfessorDTO build(Professor professor) {
        return ProfessorDTO.builder()
                .cpf(professor.getCpf())
                .name(professor.getName())
                .email(professor.getEmail())
                .phone(professor.getPhone())
                .degreeOfTraining(professor.getDegreeOfTraining())
                .build();
    }

}