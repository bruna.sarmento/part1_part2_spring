package br.ufal.lccv.treinamento.bruna_SpringBootHibernate.services;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.models.Course;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.inputs.CourseInput;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.repositories.CourseRepository;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.dtos.CourseDTO;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.builders.CourseBuilder;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.models.Student;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.dtos.StudentDTO;

import java.util.List;
import java.util.UUID;


@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))

public class CourseService {

    private final CourseRepository courseRepo;
    

    public List<CourseDTO> getAllCourse(){
        return courseRepo.findAll();
    }

    public CourseDTO getOneCourse(String name){
        List<CourseDTO> getAllCourse = courseRepo.findAll();
        for (CourseDTO course : getAllCourse) {
            if(course.getCourse() == name){
                return course;
        }   
        }
        return null;
    }

    public Course addCourse(CourseInput courseInput) {
        Course course = CourseBuilder.build(courseInput);
        course = courseRepo.save(course);

        return course;
    }

    public List<StudentDTO> getAllStudents(UUID id){
        CourseDTO course = courseRepo.getReferenceById(id);
        return course.getStudents();
    }

    public Student getCourseStudent(UUID id){
        CourseDTO course = courseRepo.getReferenceById(id);
        Student student = course.getStudent();
        return student;
    }     

}
