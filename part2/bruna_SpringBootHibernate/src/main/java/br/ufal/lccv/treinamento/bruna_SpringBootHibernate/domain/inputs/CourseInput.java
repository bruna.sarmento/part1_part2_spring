package br.ufal.lccv.treinamento.bruna_SpringBootHibernate.domain.inputs;


import javax.validation.constraints.NotBlank;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
@Schema(description = "Representação de entrada de novo curso.")

public class CourseInput {
    
    @Schema(description = "Nome do curso.")
    @NotBlank(message = "Nome não pode ser vazio!")
    private String name;

    @Schema(description = "Coordenador do curso.")
    @NotBlank(message = "Coordenador não pode ser vazio")
    private String coordinatorId;
   
    @Schema(description = "Data de Criação do curso.")
    @NotBlank(message = "Data de Criação não pode ser vazio")
    private String CreationDate;
}

