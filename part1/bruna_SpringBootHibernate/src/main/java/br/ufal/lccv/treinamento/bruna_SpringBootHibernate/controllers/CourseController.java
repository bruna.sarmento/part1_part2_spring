package br.ufal.lccv.treinamento.bruna_SpringBootHibernate.controllers;

import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.models.dtos.Course;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.services.CourseService;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/courses")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))

public class CourseController {
    
    private final CourseService service;
    
    @GetMapping
    public List<Course> getAllCourse() {
        return service.getAllCourse();
    }

    @GetMapping("/course_name/{name}")
    public Course getOneCourse(@PathVariable String name) {
       return service.getOneCourse(name);
    }

    @PostMapping("/add")
    public Course addCourse(
        @Valid 
        @RequestBody 
        Course course) {
        return service.addCourse(course);
    } 
  
}
