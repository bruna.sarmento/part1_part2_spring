package br.ufal.lccv.treinamento.bruna_SpringBootHibernate.services;

import org.springframework.stereotype.Service;

import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.models.dtos.Course;

import java.util.List;
import java.util.LinkedList;

@Service
public class CourseService {

    private List<Course> courseList = new LinkedList<>();
    
    public List<Course> getAllCourse(){
        return courseList;
    }

    public Course getOneCourse(String name){
        for (Course course : courseList) {
            if(course.getName() == name){
                return course;
        }   
        }
        return null;
    }

    public Course addCourse(Course course) {
        courseList.add(course);

        return course;
    }

}
