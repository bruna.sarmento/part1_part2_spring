package br.ufal.lccv.treinamento.bruna_SpringBootHibernate.controllers;


import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.models.dtos.Professor;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.services.ProfessorService;

import lombok.RequiredArgsConstructor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/professors")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))


public class ProfessorController {
       
    private final ProfessorService service;

    @GetMapping
    public List<Professor> getAllProfessor() {
        return service.getAllProfessor();
    }

    @GetMapping("/CPF/{cpf}")
    public Professor getOneProfessor(@PathVariable String cpf) {
       return service.getOneProfessor(cpf);
    }

    @PostMapping("/add")
    public Professor addProfessor(
        @Valid 
        @RequestBody 
        Professor professor) {
        return service.addProfessor(professor);
    }

}
