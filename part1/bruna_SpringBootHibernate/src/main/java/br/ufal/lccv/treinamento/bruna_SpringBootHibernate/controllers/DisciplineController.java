package br.ufal.lccv.treinamento.bruna_SpringBootHibernate.controllers;

import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.models.dtos.Discipline;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.services.DisciplineService;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/disciplines")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))

public class DisciplineController {
 
    private final DisciplineService service;
    
    @GetMapping
    public List<Discipline> getAllDiscipline() {
        return service.getAllDiscipline();
    }

    @GetMapping("/discipline/{name}")
    public Discipline getOneDiscipline(@PathVariable String name) {
       return service.getOneDiscipline(name);
    }

    @PostMapping("/add")
    public Discipline addDiscipline(
        @Valid 
        @RequestBody 
        Discipline discipline) {
        return service.addDiscipline(discipline);
    } 
  
    @PutMapping("/change_professor/{name}")
    public Discipline changeProfessorDiscipline(
        @PathVariable String name, 
        @RequestBody String professorId){
            return service.changeProfessorDiscipline(name, professorId);
    }

}
