package br.ufal.lccv.treinamento.bruna_SpringBootHibernate.controllers;

import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.models.dtos.Student;
import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.services.StudentService;

import lombok.RequiredArgsConstructor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/students")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))

public class StudentController {
    
    private final StudentService service;

    @GetMapping
    public List<Student> getAllStudent() {
        return service.getAllStudent();
    }

    @GetMapping("/CPF/{cpf}")
    public Student getOneStudent(@PathVariable String cpf) {
       return service.getOneStudent(cpf);
    }

    @PostMapping("/add")
    public Student addStudent(
        @Valid 
        @RequestBody 
        Student student) {
        return service.addStudent(student);
    }

    @PutMapping("/{cpf}/{courseId}")
    public Student changeStudentCourse(@PathVariable String cpf, @PathVariable String courseId) {
       return service.changeStudentCourse(cpf, courseId);
    }
   

}
