package br.ufal.lccv.treinamento.bruna_SpringBootHibernate.models.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class Course {

    @NotBlank(message = "Nome não pode ser vazio!")
    private String name;

    @NotBlank(message = "Coordenador não pode ser vazio")
    private String coordinatorId;

    @NotBlank(message = "Data de Criação não pode ser vazio")
    private String CreationDate;

    private String courseId;
}
