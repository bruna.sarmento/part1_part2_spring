package br.ufal.lccv.treinamento.bruna_SpringBootHibernate.models.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Email;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class Student {

    @NotBlank(message = "Nome não pode ser vazio!")
    private String name;

    @NotBlank(message = "CPF não pode ser vazio!")
    @Pattern(regexp = "^\\d{3}\\.\\d{3}\\.\\d{3}-\\d{2}$")
    private String cpf;

    @Email
    private String email;

    @Pattern(regexp = "^(\\d{2}\\)-\\d{5}\\-\\d{4}$")
    @NotBlank(message = "Celular não pode ser vazio")
    private String phone;

    private String courseId;

    private String studentId;
}
