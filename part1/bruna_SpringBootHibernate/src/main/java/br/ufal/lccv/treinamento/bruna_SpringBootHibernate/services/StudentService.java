package br.ufal.lccv.treinamento.bruna_SpringBootHibernate.services;

import org.springframework.stereotype.Service;

import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.models.dtos.Student;

import java.util.LinkedList;
import java.util.List;

@Service

public class StudentService {
    
    private List<Student> Students = new LinkedList<>();
        
    public List<Student> getAllStudent(){
        return Students;
    }

    public Student getOneStudent(String cpf){
        for (Student student : Students) {
            if(student.getCpf().equals(cpf)){
                return student;
        }   
        }
        return null;
    }

    public Student addStudent(Student student) {
        Students.add(student);
        return student;
    }
   
    public Student changeStudentCourse(String cpf, String courseId){
        for (Student student : Students) {
            if(student.getCpf().equals(cpf)){
                student.setCourseId(courseId);
                return student;
        }   
        }
        return null;
    }

}
