package br.ufal.lccv.treinamento.bruna_SpringBootHibernate.services;


import org.springframework.stereotype.Service;

import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.models.dtos.Professor;

import java.util.LinkedList;
import java.util.List;

@Service


public class ProfessorService {
    

    private List<Professor> Professors = new LinkedList<>();
        
    public List<Professor> getAllProfessor(){
        return Professors;
    }

    public Professor getOneProfessor(String cpf){
        for (Professor professor : Professors) {
            if(professor.getCpf().equals(cpf)){
                return professor;
        }   
        }
        return null;
    }

    public Professor addProfessor(Professor professor) {
        Professors.add(professor);
        return professor;
    }


}
