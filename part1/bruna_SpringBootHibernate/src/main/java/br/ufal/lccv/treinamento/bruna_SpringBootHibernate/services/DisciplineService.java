package br.ufal.lccv.treinamento.bruna_SpringBootHibernate.services;


import org.springframework.stereotype.Service;

import br.ufal.lccv.treinamento.bruna_SpringBootHibernate.models.dtos.Discipline;

import java.util.List;
import java.util.LinkedList;

@Service

public class DisciplineService {
    

    private List<Discipline> disciplineList = new LinkedList<>();
    
    public List<Discipline> getAllDiscipline(){
        return disciplineList;
    }

    public Discipline getOneDiscipline(String name){
        for (Discipline discipline : disciplineList) {
            if(discipline.getName() == name){
                return discipline;
        }   
        }
        return null;
    }

    public Discipline addDiscipline(Discipline discipline) {
        disciplineList.add(discipline);

        return discipline;
    }

    public Discipline changeProfessorDiscipline(String name, String professorId){
        for (Discipline discipline : disciplineList) {
            if(discipline.getName() == name){
                discipline.setProfessorId(professorId);
                return discipline;
        }   
        }
        return null;
    }

}
