package br.ufal.lccv.treinamento.bruna_SpringBootHibernate.models.dtos;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor


public class Discipline {
  
    @NotBlank(message = "Nome não pode ser vazio!")
    private String name;

    @Min(value = 30, message = "A disciplina precisa ter mais de 30 horas")
    private Integer workload;

    @NotBlank(message = "Sala de Aula não pode ser vazio")
    private int Classroom;

    private String description;

    private String professorId;

    private String disciplineId;
}
